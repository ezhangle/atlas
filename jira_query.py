	# Really simple script I use to format query results into a JIRA or 
# Confluence Ready Markdown snippet containing a table
# By Chris Sprance

from atlas_db_class import AtlasDbClass
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(filename='logs/table_creator.log',level=0, format='')


def main():
	db = AtlasDbClass(database_name='atlas.db')
	query = 'SELECT Path FROM CGF WHERE Path LIKE "%Objects/natural/%" and node_names NOT like "%branch1%" and Path not like "%_lod%" and Path NOT like "%Objects/natural/Rocks%" and Path NOT LIKE "%Objects/natural/trees%" and Path NOT LIKE "%Objects/natural/vegetation/Trees%" and Path NOT LIKE "%Objects/natural/Caves/Cave_Set_WIP1%";'
	db.format_results(query, tables=['Filepath'])

if __name__ == '__main__':
	main()

