from atlas_db_class import AtlasDbClass
from materialparser import MaterialParser
from layerparser import LayerParser
from prefabparser import PrefabParser
from textureparser import TextureParser
from cgfparser import CgfParser
import os
import sys
import argparse
import logging
import threading

logger = logging.getLogger(__name__)
logging.basicConfig(filename='logs/atlas_errors.log',level=logging.ERROR, format='')


##
##
## Need To Do Some Checking to make sure it's a file or a folder still because an empty folder is fine but a non 
# existent file is not!!!


def parse_mats(db, path):
	logger.error( "=============================Parsing Materials===============================")
	mp = MaterialParser(db)
	final_path = str(path[0].mat_path)
	
	if os.path.exists(final_path) and os.path.isfile(final_path):
		mp.material_file = final_path
	else:
		mp.material_folder =  final_path

	mp.start_parsing()
		

	
def parse_layers(db, path):
	logger.error( "=============================Parsing Layers===============================")
	lp = LayerParser(db)
	final_path = str(path[0].layer_path)
	
	if os.path.exists(final_path) and os.path.isfile(final_path):
		lp.layer_file = final_path
	else:
		lp.layer_folder =  final_path

	lp.start_parsing()
		


def parse_prefabs(db, path):
	logger.error( "=============================Parsing Prefabs===============================")
	pp = PrefabParser(db)
	final_path = str(path[0].prefab_path)
	
	if os.path.exists(final_path) and os.path.isfile(final_path):
		pp.prefab_file = final_path
	else:
		pp.prefab_folder = final_path

	pp.start_parsing()
		

	
def parse_textures(db, path):
	logger.error( "=============================Parsing Textures===============================")
	tp = TextureParser(db)
	final_path = str(path[0].texture_path)
	
	if os.path.exists(final_path) and os.path.isfile(final_path):
		tp.texture_file = final_path
	else:
		tp.texture_folder =  final_path

	tp.start_parsing()
		


def parse_cgf(db, path):
	logger.error( "=============================Parsing CGF Files===============================")
	cp = CgfParser(db)
	final_path = str(path[0].cgf_path)
	
	if os.path.exists(final_path) and os.path.isfile(final_path):
		cp.file = final_path
	else:
		cp.folder =  final_path

	cp.start_parsing()


		
def main():
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument('db_path', default='', type=str)
		parser.add_argument('mat_path', default='', type=str)
		parser.add_argument('layer_path', default='', type=str)
		parser.add_argument('prefab_path', default='', type=str)
		parser.add_argument('texture_path', default='', type=str)
		parser.add_argument('cgf_path', default='', type=str)
		args = parser.parse_args()

		# Start Our Threads
		thread_list = [parse_mats, parse_layers, parse_prefabs, parse_textures, parse_cgf]
		for thread in thread_list:
			db = AtlasDbClass(database_name=str(args.db_path), use_p4=True)
			t = threading.Thread(target=thread, args=(db,[args]))
			t.start()

	except Exception, e:
		logger.error('Database Error', e);
		exit()

	# Ok All finished up everything went well. Rename our stuff.
	# os.rename(r'atlas.db',r'atlas_prev.db' )
	# os.rename(r'temp.db',r'atlas.db' )

if __name__ == '__main__':
	main()