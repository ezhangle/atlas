#!/usr/bin/env python
# title           :layerparser.py
# description     :
# author          :Chris Sprance / Entrada Interactive
# date            :06/02/15
# usage           : This Script will be called from within the Atlas UI
# This Script parses a Crytek .lyr file and inserts it into a sqlite3 db
# using the AtlasDbClass from atlas_db_class
# notes           : 
# python_version  :2.7.5
# ==============================================================================

import xml.etree.ElementTree as ET
import logging
import os


class LayerParser(object):
    """This class contains all the methods to parse Crytek .mtl files into a dictionary to insert"""
    def __init__(self, db):
        super(LayerParser, self).__init__()
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(filename='logs/layerparser.log',level=logging.ERROR, format='')
        self.db = db
        self.layer_file = str()
        self.p4 = self.db.p4 if self.db.use_p4 else None
        self.layer_folder = str()
        self.db.commit_changes = True


    def start_parsing(self):
        '''Start Parsing The layer or layers'''
        if len(self.layer_folder) > 1:
            self.open_layers_from_folder()
        else:
            self.filter_layer()
        # after everything has been parsed and inserted commit all the changes to the database
        if self.db.commit_changes is True:
            self.db.conn.commit()


    def filter_layer(self):
        """
        Here we filter the layer send the layer info to be stored
        Then we filter the contents
        if it's a group we need to find all the Object tags inside of the Objects tag
        otherwise just send each object off to be fully parsed

        """
        layer_etree = self.parseXML()
        layers = layer_etree.find('Layer')
        layer_attribs=  {k.lower(): v.lower() for k, v in layers.attrib.iteritems()}
        # create our query dict from the Layer tags atttributes
        self.db.query_dict = self.db.start_insert(layer_attribs)
        # Send it off to the db
        self.db.build_query('Layers')
        # Layerobejcts contains oall the Object tags
        layer_objects = layers.find('LayerObjects')
        # The object tag contains information for any placed asset
        object_list = layer_objects.findall('Object')
        # clear the dictionary
        self.db.query_dict.clear()

        # Loop through the object_list
        for object in object_list:
            # don't forget to check that the list is > 0
            if object.attrib['Type'] == 'Group' and len(object.getchildren()) > 0:
                # insert the group object into the db in the Groups table
                self.db.query_dict = self.db.start_insert(object.attrib)
                self.db.build_query('Groups')
                # get the children of the group object
                child_list = object.getchildren()
                # Find all the object tags inside of objects
                child_list = [child.findall('Object') for child in child_list][0]
                # clear the query dict
                self.db.query_dict.clear()
                # loop through the child_list and insiert them into the dict
                for child in child_list:
                    self.db.query_dict.clear()
                    self.recurse_children(child)
                    self.db.query_dict = self.db.start_insert(self.db.query_dict)
                    self.db.build_query(self.db.query_dict['type'][0])
            else:
                # Loop through the object and insert it into self.db.query_dict
                self.db.query_dict.clear()
                # get all the  attribs in all the children
                self.recurse_children(object)
                # stick it into the db with the Type as the table name
                self.db.query_dict = self.db.start_insert(self.db.query_dict)
                self.db.build_query(self.db.query_dict['type'][0])
    

    # recurse children function
    def recurse_children(self, obj):
            #process the obj.attrib and add them to a dict
            for attrib in obj.attrib:
                # if the key exists append something unique to it
                if self.db.query_dict.has_key(attrib):
                    self.db.query_dict[attrib.lower() + '_' + obj.tag.lower()] = obj.attrib[attrib]
                else:
                    self.db.query_dict[attrib.lower()] = obj.attrib[attrib]
            # if the obj has any children send each child through
            if len(obj.getchildren()) > 0:
                # loop through all the children and add there attributes in to query dict
                for child in obj.getchildren():
                    self.recurse_children(child)


    def parseXML(self):
        """Parse the xml and return the xml tree back to us"""
        # lg.info( self.material_file)
        xml_file = open(self.layer_file)
        tree = ET.parse(xml_file).getroot()
        return tree
        

    def open_layers_from_folder(self):
        '''This method contains the code necesarry to open all .lyr if recursive arg is set to true it will recurse through all subdirectories'''
        for root, dirs, files in os.walk(self.layer_folder):
            for file in files:
                if file.endswith(".lyr"):
                    self.layer_file = os.path.join(root, file)
                    try:
                        self.filter_layer()
                    except Exception, e:
                        self.logger.error( 'Cannot open Layer File: %s Error: %s' % (self.layer_file, e))
                        pass

                        
def main():
    pass
    # lp = LayerParser()
    
    # lp.layer_file = r'D:\perforce\GameSDK\Levels\islands\layers\Cave_system3.lyr'

    # lp.layer_folder = 'D:\perforce\GameSDK\Levels\islands\layers'
    
    # kick off everything
    # lp.start_parsing()


if __name__ == '__main__':
    main()
