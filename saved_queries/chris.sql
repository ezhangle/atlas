/*Select any bad lights in the map*/
SELECT `radius` , `name`, `pos`, * FROM `Entity` WHERE `EntityClass` LIKE 'Light' ORDER BY `radius` DESC;
/*select any lights with huge radius*/
SELECT `radius` , `name`, `pos`, * FROM `Entity` WHERE `EntityClass` LIKE 'Light' AND CAST(`radius` as INTEGER) > 70 ORDER BY `radius` DESC;
/*Generic Prefab selection by name*/
SELECT * FROM `Prefab` WHERE `prefabname` LIKE 'Residential.the_last_house.Red_Normal_Matt';
/*Select anything at the origin point*/
SELECT * FROM `Brush`, `Prefab`, `Entity`, `ArchetypeEntity`, `ParticleEntity` WHERE `pos` LIKE '0,0,0';
/*Select all the textures with Supress Engine Reduce on*/
SELECT * FROM Textures WHERE ser=1
/*Find all the big textures with no reduction*/
select * from textures where image_width > 2048 and reduce like "0"
/*Find all the veggie mats with PHONG_TESSELLATION or PN_TESSELLATION */
SELECT * FROM Materials where Shader like 'Vegetation' and StringGenMask like '%PHONG_TESSELLATION%' or Shader like 'Vegtation' and StringGenMask like '%PN_TESSELLATION%'
/*Select any materials that have a diffuse texture like x */
SELECT texture_Diffuse, filepath,  * FROM Materials WHERE texture_Diffuse LIKE "objects/structures/residential/onestoryhouse/%"
/*Select any materials with no Surface Types*/
SELECT * FROM Materials WHERE SurfaceType = '' and issubmat like "True"
/* Select all the mutant spawners */
SELECT pos FROM Brush WHERE Prefab Like "Objects/spawners/spiker.cgf"
/*Deer spawners*/
SELECT pos FROM Brush WHERE Prefab Like "Objects/spawners/randomdeer.cgf"
SELECT pos FROM Brush where prefab LIKE "Objects/spawners/f100truck.cgf";
SELECT pos FROM Brush where prefab LIKE "Objects/spawners/towcar.cgf";
SELECT pos FROM Brush where prefab LIKE "Objects/spawners/quadbike.cgf";
/*Player Spawners*/
Select pos FROM Entity WHERE EntityClass = "SpawnPoint";
/*select something that has no parents*/
SELECT pos FROM `Brush` WHERE Prefab Like "Objects/spawners/randommelee.cgf" AND `parent` IS NULL
/*SElect all the lights not in prefabs*/
Select pos FROM Entity WHERE EntityClass = "Light" AND parent IS NULL;

/*FIND hi poly lodless meshes*/
SELECT * from CGF WHERE lod1 IS NULL AND rendermesh > 20000;
/*FIND lodless meshes*/
SELECT * from CGF WHERE lod1 IS NULL;
/*Show all tables in the db*/
SELECT name FROM sqlite_master WHERE type = "table";
/*Show all the columns in the table*/
PRAGMA table_info(Brush);