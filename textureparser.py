#!/usr/bin/env python
# title           :parsetexture.py
# description     :This script parses Crytek .tiff files and inserts them into # a sqlite3 database
# author          :Chris Sprance / Entrada Interactive
# date            :05292015
# usage           : This Script will be called from within the Atlas UI
# You pass it a texture file or a folder and it will parse the texture or
# parse every folder recursively it can find and adds them all to the
# database
# notes           : 
# python_version  :2.7.5
# ==============================================================================
import os
import re
import logging
import re
import tifffile
from atlas_db_class import AtlasDbClass


class TextureParser(object):
    """docstring for TextureParser"""
    def __init__(self, db):
        super(TextureParser, self).__init__()
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(filename='logs/textureparser.log',level=logging.ERROR, format='')
        self.db = db
        self.db.commit_changes = True
        self.texture_file = str()
        self.texture_folder = str()
        self.p4 = self.db.p4 if self.db.use_p4 else None

    def strip_control_characters(self, input):
        if input:
            # unicode invalid characters
            RE_XML_ILLEGAL = u'([\u0000-\u0008\u000b-\u000c\u000e-\u001f\ufffe-\uffff])' + \
                             u'|' + \
                             u'([%s-%s][^%s-%s])|([^%s-%s][%s-%s])|([%s-%s]$)|(^[%s-%s])' % \
                (unichr(0xd800), unichr(0xdbff), unichr(0xdc00), unichr(0xdfff),
                 unichr(0xd800), unichr(0xdbff), unichr(
                    0xdc00), unichr(0xdfff),
                 unichr(0xd800), unichr(0xdbff), unichr(
                    0xdc00), unichr(0xdfff),
                 )
            input = re.sub(RE_XML_ILLEGAL, "", input)
            # ascii control characters
            input = re.sub(r"[\x01-\x1F\x7F]", "", input)
            input = re.sub(r"(8BIM...)", "", input)
        # return raw values from a tag
        return input

    def get_compiled_size(self):
        # get the compiled size
        tex_sizes =[2,4,8,16,32,64,128,256,512,1024,2048,4096,8192]
        # try to trun it into an int
        try:
            reduce_amount = int(self.db.query_dict['reduce'])
            self.db.query_dict['dds_width'] = tex_sizes[tex_sizes.index(int(self.db.query_dict['image_width']))-reduce_amount]
            self.db.query_dict['dds_length'] = tex_sizes[tex_sizes.index(int(self.db.query_dict['image_length']))-reduce_amount]
        except:
            reduce_list =  self.db.query_dict['reduce'].split(',')
            idx = [i for i, item in enumerate(reduce_list) if item.startswith('pc:')][0]
            reduce_amount = int(reduce_list[idx].split(':')[-1])
            if reduce_amount < 0:
                reduce_amount = 0
            self.db.query_dict['dds_width'] = tex_sizes[tex_sizes.index(int(self.db.query_dict['image_width']))-reduce_amount]
            self.db.query_dict['dds_length'] = tex_sizes[tex_sizes.index(int(self.db.query_dict['image_length']))-reduce_amount]



    def extract_iptc_data(self, img):
        # some stuff doesn't have metadata for whatever reason ()
        try:
            # open the file
            im = tifffile.TiffFile(img)
            # extract the date
            iptc = im.pages[0].tags['photoshop'].value.tostring()
            # split the string into list and remove the control characters
            iptc_list = self.strip_control_characters(iptc).split()
            self.logger.debug(iptc_list)
            # turn it into a dictionart
            self.db.query_dict = dict([data.replace('/','').split('=') for data in iptc_list])
            if self.db.use_p4:
                self.db.query_dict['imagepath'] = self.p4.run('fstat',img)[0]['depotFile']
            else:
                self.db.query_dict['imagepath'] = self.texture_file
                
            self.db.query_dict['image_width'] = str(im.pages[0].tags['image_width'].value)
            self.db.query_dict['image_length'] = str(im.pages[0].tags['image_length'].value)

            # grab the dds size
            self.get_compiled_size()

            for x in self.db.query_dict:
                self.db.query_dict[x] = [self.db.query_dict[x], self.db.determine_sql_datatype(self.db.query_dict[x])]

            # Build our query
            self.db.build_query('Textures')

        except Exception, e:
            self.logger.error(e)
            raise e

    def start_parsing(self):
         # recurse through all directors only finding files with .tif
        if len(self.texture_file) > 1:
            self.extract_iptc_data(self.texture_file)
        else:
            directory = self.texture_folder
        # go through the folder and grab only .tiff images and send them off
            for root, dirs, files in os.walk(directory):
                for file in files:
                    if file.endswith(".tif"):
                        try:
                            self.texture_file = os.path.join(root, file)
                            self.extract_iptc_data(self.texture_file)
                        except Exception, e:
                            self.logger.error("Error on %s Error Raised: %s" % (file, e))
        if self.db.commit_changes:
            self.db.conn.commit()

# main


def main():
    db = AtlasDbClass(database_name='atlas.db', use_p4=True)
    tp = TextureParser(db)
    # tp.texture_file = "D:/perforce/dev/GameSDK/Objects/ammo/556_nato/556_nato_ddna.tif"
    tp.texture_file = r"null"
    # tp.texture_folder = "null"
    tp.start_parsing()




# run the shizzle
if __name__ == '__main__':
    main()
