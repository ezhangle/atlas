# Atlas Reporting System

Atlas report system reads from layers and inserts data about entities, brushes, vegetation objects and other various data points.

The database is kept under version control and updated manually with each build. However at any time the user can overwrite the database by importing a layer. This allows artists to make changes reload the layer and check to make sure the changes have been completed.

The database is a SQLlite3 database which means you get quick scalable solution to search through all objects and gather data from them.

Within the application you can store SQL statements and execute them as well as write a random sql statement and execute it. The application also comes with a list of common troubleshooting sql statements.

A CLI Tool exists to quickly parse data


# Usage:


```
#!python

python rebuild_db.py [db_path] [mat_path] [layer_path] [prefab_path] [texture_path] [cgf_path]
```



# Production Command


```
#!python

python rebuild_db.py atlas.db D:\perforce\dev\GameSDK D:\perforce\dev\GameSDK\Levels\islands D:\perforce\dev\GameSDK\Prefabs D:\perforce\dev\GameSDK D:\perforce\dev\GameSDK
```



# Testing Command


```
#!python

python rebuild_db.py testing.db D:\perforce\dev\GameSDK\Objects\props\bathroomMirror D:\perforce\dev\GameSDK\Levels\islands\layers\alex_group.lyr D:\perforce\dev\GameSDK\Prefabs\atlas_test_prefab.xml D:\perforce\dev\GameSDK\Textures\Wood\floor_board_01_diff.tif D:\perforce\dev\GameSDK\Objects\magazines\stanagx30
```



# When using the classes directly:

Every class works by first setting either a folder or a specific file to start parsing and inserting into the databse.

so usually 

```
#!python

pp.file = filepath
pp.folder = folder
```



then you always call the 

```
#!python
pp.start_parsing() 
```
and it will start either going through the folder and parsing data or from the specific file.

Error logs are dumped into 
logs/

The db class contains all the methods needed to insert objects from the individual Parsing classes.

P4 is used to make sure that nothing that isn't in the repo gets added into the database.

-------------
From the front end of the database system you can execute sql queries as well as pull up a map to visualize any sql query that uses the pos db field.

So like

```
#!sql
SELECT pos from Brush where Prefab="test.cgf"
```
This returns a bunch of vec3 data which gets used by leaflet to place markers on a map.


You can also just execute regular sql queries and view them. This way people can begin to create there own burn down charts and JIRA issues based on the queries.

Would be nice to be able to execute a query then from a row creat a JIRA issue from the data within the row


-------------

Help on AtlasDbClass in module atlas_db_class object:

```
#!python


Help on AtlasDbClass in module atlas_db_class object:

class AtlasDbClass(__builtin__.object)
 |  This Class takes columns and values and adds them to a database and handles the errors
 |  when you use a self.build_query("Table") it will build the query and execute the query
 |  Queries can be made directly using db_exec_raw()
 |  
 |  Methods defined here:
 |  
 |  __init__(self, database_name='atlas.db', primary_key='')
 |  
 |  alter_table(self, column, table)
 |      This alters the tables
 |  
 |  build_query(self, table)
 |      Takes a dictionary and creates a sqllite3 query out of it which it sends to db_exec to handle errors
 |      Dictionary location is self.query_dict
 |      
 |      {'ColumnName':['value', 'datatype']}
 |      
 |      self.db.query_dict = {'TestPrimaryColumn':['TestValue',' TEXT'],'OtherColumn':['OtherValue',' TEXT']}
 |      
 |      This is the priamry key for the table
 |      self.db.primary_key = 'TestPrimaryColumn'
 |      build your query by sending it the table you want to insert that data in to
 |      self.db.build_query('Test')
 |  
 |  clean_list(self, dirty_list, values)
 |      clean_list(dirty_list, values_list)
 |      This takes a dirty list with duplicate values and 
 |      removes any duplicates from the first list while 
 |      preserving order and removes the same elements 
 |      from the second list
 |  
 |  create_table(self, table)
 |      This creates the database tables and column names
 |  
 |  db_exec(self, query, table='')
 |  
 |  db_exec_raw(self, query)
 |      This just takes a query and executes it directly no error checking
 |      If the Query succeeds it returns True
 |      if the query fails it returns with the error message
 |  
 |  determine_sql_datatype(self, value)
 |  
 |  qstring(self, string)
 |      returns a string with single quotes around it
 |  
 |  start_insert(self, qdict)
 |      Goes through a dict and figures out all the data types and adds the list to the dict
 |      Returns a dictionary like {'dictkey': ['dict_value', ' INTEGER'], 'dict2':['dict_value', ' TEXT']}
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)


Help on MaterialParser in module materialparser object:

class MaterialParser(__builtin__.object)
 |  This class contains all the methods to parse Crytek .mtl files into a dictionary to insert
 |  
 |  Methods defined here:
 |  
 |  __init__(self)
 |  
 |  create_query_dict_from_material(self, material)
 |  
 |  filter_material(self)
 |      If it's a submaterial we need to find all the material tags inside of the submaterial tag and send those to be added
 |  
 |  open_materials_from_folder(self)
 |      This method contains the code necesarry to open all .lyr if recursive arg is set to true it will recurse through all subdirectories
 |  
 |  parseXML(self)
 |      Parse the xml and return the xml tree back to us
 |  
 |  start_parsing(self)
 |      Start Parsing The layer or layers
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)


Help on LayerParser in module layerparser object:

class LayerParser(__builtin__.object)
 |  This class contains all the methods to parse Crytek .mtl files into a dictionary to insert
 |  
 |  Methods defined here:
 |  
 |  __init__(self)
 |  
 |  filter_layer(self)
 |      Here we filter the layer send the layer info to be stored
 |      Then we filter the contents
 |      if it's a group we need to find all the Object tags inside of the Objects tag
 |      otherwise just send each object off to be fully parsed
 |  
 |  open_layers_from_folder(self)
 |      This method contains the code necesarry to open all .lyr if recursive arg is set to true it will recurse through all subdirectories
 |  
 |  parseXML(self)
 |      Parse the xml and return the xml tree back to us
 |  
 |  recurse_children(self, obj)
 |      # recurse children function
 |  
 |  start_parsing(self)
 |      Start Parsing The layer or layers
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)


Help on PrefabParser in module prefabparser object:

class PrefabParser(__builtin__.object)
 |  This class contains all the methods to parse Crytek Prefab.xml files into a dictionary and send it off 
 |  to AtlasDbClass
 |  
 |  Methods defined here:
 |  
 |  __init__(self)
 |  
 |  filter_prefab(self)
 |  
 |  open_prefabs_from_folder(self)
 |      This method contains the code necesarry to open all Prefab.xml files
 |      from a folder or a file
 |  
 |  parseXML(self)
 |      Parse the xml and return the xml tree back to us
 |  
 |  start_parsing(self)
 |      Start Parsing The prefab_folder or prefab_file also 
 |      determines whether or not to commit the data
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)


Help on TextureParser in module textureparser object:

class TextureParser(__builtin__.object)
 |  docstring for TextureParser
 |  
 |  Methods defined here:
 |  
 |  __init__(self)
 |  
 |  extract_iptc_data(self, img)
 |  
 |  start_parsing(self)
 |  
 |  strip_control_characters(self, input)
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)


Help on CgfParser in module cgfparser object:

class CgfParser(__builtin__.object)
 |  This class contains all the methods to parse a Crytek geometry filetypeand give us relevant datapoints
 |  
 |  Methods defined here:
 |  
 |  __init__(self)
 |  
 |  filter(self)
 |      Here we filter the geometry file using chunkfile send the chunks off to be parsed
 |  
 |  open_from_folder(self)
 |      This method contains the code necesarry to open all geometry files individualy or from a folder
 |  
 |  start_parsing(self)
 |      Start Parsing The geometry file or files
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)


```