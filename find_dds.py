import os

def open_layers_from_folder(folder):
    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.endswith(".dds"):
                dds_file = os.path.join(root, file)
                print dds_file

def main():
    open_layers_from_folder(r'D:/perforce/dev/GameSDK')

if __name__ == '__main__':
    main()