#!/usr/bin/env python
# title           :prefabparser.py
# description     :
# author          :Chris Sprance / Entrada Interactive
# date            :06/02/15
# usage           : This Script will be called from within the Atlas UI
# This Script parses a Crytek Prefab.xml file and inserts it into a sqlite3 db
# using the AtlasDbClass from atlas_db_class
# notes           : 
# python_version  :2.7.5
# ==============================================================================

import xml.etree.ElementTree as ET
import logging
import os



class PrefabParser(object):
	"""
	This class contains all the methods to parse Crytek Prefab.xml files into a dictionary and send it off 
	to AtlasDbClass
	"""
	def __init__(self, db):
		super(PrefabParser, self).__init__()
		self.db = db
		self.logger = logging.getLogger(__name__)
		logging.basicConfig(filename='logs/prefabparser.log',level=logging.ERROR, format='')
		self.prefab_file = str()
		self.prefab_folder = str()
		self.db.commit_changes = True
		self.p4 = self.db.p4 if self.db.use_p4 else None

	def start_parsing(self):
		'''
		Start Parsing The prefab_folder or prefab_file also 
		determines whether or not to commit the data
		'''
		if len(self.prefab_folder) > 1:
		    self.open_prefabs_from_folder()
		else:
		    self.filter_prefab()
		# after everything has been parsed and inserted commit all the changes to the database
		if self.db.commit_changes is True:
		    self.db.conn.commit()

	def parseXML(self):
		"""Parse the xml and return the xml tree back to us"""
		self.logger.info(self.prefab_file)
		xml_file = open(self.prefab_file)
		tree = ET.parse(xml_file).getroot()
		return tree

	def filter_prefab(self):
			# parse the prefab_file
		def recurse_children(obj):
			#process the obj.attrib and add them to a dict
			for attrib in obj.attrib:
				# if the key exists append something unique to it
				if self.db.query_dict.has_key(attrib):
					self.db.query_dict[attrib + '_' + obj.tag] = obj.attrib[attrib]
				else:
					self.db.query_dict[attrib] = obj.attrib[attrib]
			# if the obj has any children send each child through
			if len(obj.getchildren()) > 0:
				# loop through all the children and add there attributes in to query dict
				for child in obj.getchildren():
					recurse_children(child)
		try:
			prefab_file = self.parseXML()

			# find all the prefab inside of prefabs file
			prefabs =  prefab_file.findall('Prefab')

			#
			#  Insert Prefabs
			#
			# loop through the prefabs
			for prefab in prefabs:
				# gather all the objects
				objects = prefab.find('Objects').findall('Object')
				
				# set the query dict to be our prefab.attrib
				qd = prefab.attrib

				# get the count of objects in the prefab
				qd['object_count'] = len(objects)

				# set the filepath of the prefab
				if self.db.use_p4:
					qd['filepath'] = self.p4.run('fstat',self.prefab_file)[0]['depotFile']
				else:
					qd['filepath'] = self.prefab_file

				#build the data types for the query_dict
				self.db.query_dict = self.db.start_insert(qd)

				# set the primary key
				self.db.primary_key = 'Id'
				Id = prefab.attrib['Id']

				# start our database insertion
				self.db.build_query('Prefabs')
				#clear our dicts
				self.db.query_dict.clear()
				for obj in objects:
					recurse_children(obj)
					# clear our dicts out
					qdict = self.db.query_dict
					qdict['prefab_id'] = Id[0]
					#create our query dict
					self.db.query_dict =  self.db.start_insert(qdict)
					# create our query and insert it into the Type key of the qdict
					self.db.build_query(self.db.query_dict['Type'][0])
					self.db.query_dict.clear()

		#
		# Stop Inserting Prefabs
		#
		#
		# Start Inserting Objects in side of Prefabs
		#
			# now we need to loop through all the objects in the prefab and add them to the appropriate table
		#
		# Stop Inserting Objects in side of Prefabs
		#
		except Exception, e:
			self.logger.error( e)



	def open_prefabs_from_folder(self):
		'''
		This method contains the code necesarry to open all Prefab.xml files
		from a folder or a file
		'''
		for root, dirs, files in os.walk(self.prefab_folder):
		    for file in files:
		        if file.endswith(".xml"):
		            self.prefab_file = os.path.join(root, file)
		            try:
		            	self.filter_prefab()
		            except Exception, e:
		            	self.logger.error( 'Cannot Parse Prefab file %s Error: %s' % (self.prefab_file, e))
		            	pass


def main():
	pass


if __name__ == '__main__':
	main()
