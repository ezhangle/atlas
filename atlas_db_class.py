#!/usr/bin/env python
# title           :atlas_db_class.py
# description     :This script takes a dictionary {'ColumnName':['value', 'datatype']}
#  								 and will add every value and colum needed to the db
# author          :Chris Sprance / Entrada Interactive
# date            :06/02/15
# usage           : This Script will be called from within the Atlas UI
# You pass it a layer file or a folder and it will parse the layer or
# parse every .lyr folder recursively it can find and adds them all to the
# database
# notes           :
# python_version  :2.7.5
# ==============================================================================
import logging
from P4 import P4
import sqlite3
import time
from ordered_set import OrderedSet


class AtlasDbClass(object):

    """
    This Class takes columns and values and adds them to a database and handles the errors
    when you use a self.build_query("Table") it will build the query and execute the query
    Queries can be made directly using db_exec_raw()
    """

    def __init__(self, database_name='atlas.db', primary_key='', use_p4=False):
        super(AtlasDbClass, self).__init__()
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(filename='logs/atlas.log',level=logging.ERROR, format='')
        self.logger.error('/*--------------------------------------------------\n'+'Starting up atlas_db_class '+ time.strftime("%c") + '\n'+'--------------------------------------------------\nSQL Queries:\n')
        self.query_dict = dict()
        self.use_p4 = use_p4
        # find out if we need to use P4
        try:
            self.p4 = P4()
            self.p4.client = 'csprance'
            self.p4.connect()
        except Exception, e:
            logging.error(e)
        self.database_name = database_name
        # database connection
        self.conn = sqlite3.connect(database_name, check_same_thread=False)
        # sqlite3 cursor
        self.c = self.conn.cursor()
        self.commit_changes = True
        self.primary_key = primary_key
        self.logger.error('Using Database: {}'.format(self.database_name) + '*/')


    def determine_sql_datatype(self, value):
        # remove any qoutes if it's a string
        value = value.replace("'", "") if not isinstance(value, int) or isinstance(value, list) else value
        if value == 0:
            return ' BOOLEAN'
        if isinstance(value, int):
            return ' INTERGER'
        else:
            return ' TEXT'
    
    def format_results(self, query, table_headers=False, tables=()):
        '''This return the values of a query in a nice formated table for JIRA
        All table headers can be output with table_headers=True
        Custom Tables can be set with tables=List()
        query is the query you want to execute
        '''
        if table_headers:
            self.db_exec_raw("PRAGMA table_info(Textures)")
            text = "||"
            for row in self.c.fetchall():
                text +=  row[1] + "||"
            print text
        
        if tables:
            text = "||"
            for x in tables:
                text += x + "||"
            print text

        self.db_exec_raw(query)
        text = "|"
        for row in self.c.fetchall():
            for data in row:
                text += str(data) + "|"
            text += "\n|"
        print text[:-1].replace("{", "").replace("}","")



    def clean_list(self, dirty_list, values):
        '''clean_list(dirty_list, values_list)
        This takes a dirty list with duplicate values and 
        removes any duplicates from the first list while 
        preserving order and removes the same elements 
        from the second list'''
        x = dirty_list
        yset = OrderedSet()
        idx_remove = OrderedSet()
        for idx, y in enumerate(x):
            if y not in yset:
                if y.lower() not in yset:
                    yset.add(y)
                    idx_remove.add(idx)
                
        cleaned_list = [i for j, i in enumerate(x) if j in idx_remove]
        values = [i for j, i in enumerate(values) if j in idx_remove]
        return cleaned_list, values


    def db_exec_raw(self, query):
        """
        This just takes a query and executes it directly no error checking
        If the Query succeeds it returns True
        if the query fails it returns with the error message
        """
        try:
            self.c.execute(query)
            self.logger.debug(query)
            return True
        except Exception, e:
            return e


    def db_exec(self, query, table=''):
        try:
            self.logger.debug(query)
            self.c.execute(query)
            if self.commit_changes:
                self.conn.commit()
        except sqlite3.Error, e:
            # conver the error message to a string
            x = str(e)

            # ID already Exists error
            if x == "column id is not unique":
                self.logger.error('/*' + x + '*/')
                return

            # this is our no such table error
            if x[0:13] == "no such table":
                self.logger.error('/*' + x + '*/')
                self.create_table(table)
                self.db_exec(query, table)

            # this is our table has no column
            if x[0:5] == 'table':
                self.logger.error('/*' + x + '*/')
                # need to find a way to eal with this error better
                error = x.split()
                column_name = error[-1]
                self.alter_table(column_name, table)
                self.db_exec(query, table)

    def alter_table(self, column, table):
        """This alters the tables """
        alter_stmt = "ALTER TABLE "+ table + " ADD COLUMN " + self.qstring(column) + self.query_dict[column][1]
        self.logger.info(alter_stmt)
        self.db_exec(alter_stmt)

    def create_table(self, table):
        '''This creates the database tables and column names'''
        columns = [key + self.query_dict[key][1] for key in self.query_dict]
        columns, values = self.clean_list(columns, columns)
        if len(self.primary_key) > 0:
            self.db_exec("CREATE TABLE IF NOT EXISTS " + table + " ( " +
                     ','.join(columns) + ",  PRIMARY KEY(" + self.primary_key + ") )")
        else:
            self.db_exec("CREATE TABLE IF NOT EXISTS " + table + " ( " +
                     ','.join(columns) + ")")

    
    def start_insert(self, qdict):
        ''' Goes through a dict and figures out all the data types and adds the list to the dict
        Returns a dictionary like {'dictkey': ['dict_value', ' INTEGER'], 'dict2':['dict_value', ' TEXT']} '''
        for key, value in qdict.items():
            qdict[key] = [value, self.determine_sql_datatype(value)]

        return qdict


    def qstring(self, string):
        '''returns a string with single quotes around it'''
        return "'{}'".format(string)

    def build_query(self, table):
        """Takes a dictionary and creates a sqllite3 query out of it which it sends to db_exec to handle errors
            Dictionary location is self.query_dict
            
            {'ColumnName':['value', 'datatype']}
            
            self.db.query_dict = {'TestPrimaryColumn':['TestValue',' TEXT'],'OtherColumn':['OtherValue',' TEXT']}
            
            This is the priamry key for the table
            self.db.primary_key = 'TestPrimaryColumn'
            build your query by sending it the table you want to insert that data in to
            self.db.build_query('Test')
            """
        columns = [self.qstring(key) for key in self.query_dict]
        values = [self.qstring(self.query_dict[value][0]) for value in self.query_dict]
        columns,values = self.clean_list(columns, values)
        
        query = "INSERT INTO " + table + " ( " + ','.join(columns) + ") VALUES ( " + ','.join(values) + ");"
        self.db_exec(query, table)


def main():
    # db = AtlasDbClass('testing.db', 'test', p4=False)
    pass

if __name__ == '__main__':
    main()
