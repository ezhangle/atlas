from PIL import Image

try:                                               #test file exists
    im = Image.open(r"c:\temp\file.tif")
except:
    print("Error opening image")

multiply = 5                                       #how much bigger
processing = tuple([multiply*x for x in im.size])  #maths
saved = (r"c:\temp\biggerfile.tif")               #save location

imB = im.resize((processing))                      #resizing

imB.save(saved) 