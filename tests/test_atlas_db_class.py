import sys, os
sys.path[0] = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', ''))

from atlas_db_class import AtlasDbClass
from materialparser import MaterialParser
from layerparser import LayerParser
from prefabparser import PrefabParser
from textureparser import TextureParser
from cgfparser import CgfParser
import logging, threading


logging.basicConfig(filename="logs/error_log.log", format="",level=0)
logger = logging.getLogger(__name__)

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

def main():
	# test the Db class
	db = AtlasDbClass(database_name='testing.db',use_p4=False)

	# # test the material parser
	mp = MaterialParser(db)
	mp.material_file = r'd:\perforce\dev\GameSDK\Materials\decals\Grass\PatchyGrass.mtl'
	# mp.start_parsing()
	
	# # # test the prefabParser()
	pp = PrefabParser(db)
	pp.prefab_file = r'prefab_test_data\Campsite.xml'
	# pp.start_parsing()

	# # # # test the CgfParser()
	cp = CgfParser(db)
	cp.file = r'cgf_test_data\flare_projectile.cgf'
	# cp.start_parsing()

	# # # # Do a test of the LayerParser class
	lp = LayerParser(db)
	lp.layer_file = r"islands\layers\Alex_cave_a.lyr"
	# lp.start_parsing()
	
	
	# # # do a test of the TextureParser()
	tp = TextureParser(db)
	tp.texture_file = r"texture_test_data\12gauge_dif.tif"
	tp.start_parsing()

	
	# # class_list = [db,mp,lp,pp,tp,cp]

	# Print out some logging info
	# logger.debug( "Database Name: AtlasDbClass: " + db.database_name)
	# logger.debug( "Database Name: MaterialParser: " + mp.db.database_name)
	# logger.debug( "Database Name: LayerParser: " + lp.db.database_name)
	# logger.debug( "Database Name: PrefabParser: " + pp.db.database_name)
	# logger.debug( "Database Name: TextureParser: " + tp.db.database_name)
	# logger.debug( "Database Name: CgfParser: " + cp.db.database_name)


	# Print out the docs
	help(db)
	help(mp)
	help(lp)
	help(pp)
	help(tp)
	help(cp)

	# threading test
	# thread_list = [db.db_exec_raw, mp.db.db_exec_raw, lp.db.db_exec_raw, pp.db.db_exec_raw, tp.db.db_exec_raw, cp.db.db_exec_raw]
	# test_query = ['SELECT * FROM TEST']
	# for thread in thread_list:
	# 	t = threading.Thread(target=thread, args=(test_query))
	# 	t.start()



if __name__ == '__main__':
	main()