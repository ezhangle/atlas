import collections


class OrderedSet(collections.MutableSet):

    def __init__(self, iterable=None):
        self.end = end = []
        end += [None, end, end]         # sentinel node for doubly linked list
        self.map = {}                   # key --> [key, prev, next]
        if iterable is not None:
            self |= iterable

    def __len__(self):
        return len(self.map)

    def __contains__(self, key):
        return key in self.map

    def add(self, key):
        if key not in self.map:
            end = self.end
            curr = end[1]
            curr[2] = end[1] = self.map[key] = [key, curr, end]

    def discard(self, key):
        if key in self.map:
            key, prev, next = self.map.pop(key)
            prev[2] = next
            next[1] = prev

    def __iter__(self):
        end = self.end
        curr = end[2]
        while curr is not end:
            yield curr[0]
            curr = curr[2]

    def __reversed__(self):
        end = self.end
        curr = end[1]
        while curr is not end:
            yield curr[0]
            curr = curr[1]

    def pop(self, last=True):
        if not self:
            raise KeyError('set is empty')
        key = self.end[1][0] if last else self.end[2][0]
        self.discard(key)
        return key

    def __repr__(self):
        if not self:
            return '%s()' % (self.__class__.__name__,)
        return '%s(%r)' % (self.__class__.__name__, list(self))

    def __eq__(self, other):
        if isinstance(other, OrderedSet):
            return len(self) == len(other) and list(self) == list(other)
        return set(self) == set(other)


def clean_list(dirty_list, values):
    '''clean_list(dirty_list, values_list)
    This takes a dirty list with duplicate values and 
    removes any duplicates from the first list while 
    preserving order and removes the same elements 
    from the second list'''
    x = dirty_list
    yset = OrderedSet()
    idx_remove = OrderedSet()
    for idx, y in enumerate(x):
        if y not in yset:
            if y.lower() not in yset:
                yset.add(y)
                idx_remove.add(idx)
            
    cleaned_list = [i for j, i in enumerate(x) if j in idx_remove]
    values = [i for j, i in enumerate(values) if j in idx_remove]
    return cleaned_list, values


def main():
    x = list()
    x.append('test1')
    x.append('test2')
    x.append('Test2')
    x.append('TeSt2')
    x.append('test3')
    x.append('test4')
    x.append('test5')
    x.append('test5')
    x.append('test5')
    x.append('test3')
    x.append('test6')
    x.append('test7')
    x.append('test8')
    x.append('test9')
    x.append('test9')
    x.append('test1')
    x.append('test9')
    y = list()
    y.append('test1')
    y.append('test2')
    y.append('test2')
    y.append('test3')
    y.append('test4')
    y.append('test5')
    y.append('test5')
    y.append('test5')
    y.append('test3')
    y.append('test6')
    y.append('test7')
    y.append('test8')
    y.append('test9')
    y.append('test9')
    y.append('test1')
    y.append('test9')
    print clean_list(x, y)

if __name__ == '__main__':
    main()
