import sys
import os
sys.path.insert(0, 'D:/perforce/Atlas')
import heatmap as hm
from PIL import Image
from atlas_db_class import AtlasDbClass


def setup_config(query):
		db = AtlasDbClass(database_name='atlas.db')
		db.c.execute(query)
		file = open('tempfile.coords', 'w')
		file.write('0.0001 0.0001\n')
		for x in db.c.fetchall():
			vec = x[0].replace(',', ' ').split(' ')
			vec2 = vec[0] + ' ' + vec[1]+ '\n'
			file.write( vec2)
		file.write('8192 8192')
		file.close()
		config = hm.Configuration()
		config.projection = hm.EquirectangularProjection()
		config.projection.pixels_per_degree = 1
		config.colormap.hsva_min = (0,0,0,1)
		config.height = 8192
		config.width = 8192
		# config.projection.extent_in = 0
		# config.projection.extent_in = 8192
		
		# world map image
		image = Image.open('world_map.png')

		#rotate the image so our pixel coordinates match our world space coordinates

		image = image.transpose(Image.FLIP_TOP_BOTTOM)
		image = image.rotate(90)
		size = 8192, 8192
		image = image.resize(size, Image.ANTIALIAS)
		config.background_image = image
		# config.background = 'white'
		# config.projection.is_scaled()
		config.decay = 0
		config.shapes =  hm.shapes_from_file('tempfile.coords')
		# this controls the intensity of the layering effect
		config.kernel = hm.LinearKernel(20)
		# config.fill_missing()
		return config

def create_heatmap(imagename, query):
	config = setup_config(query)
	matrix = hm.process_shapes(config)
	matrix = matrix.finalized()
	image = hm.ImageMaker(config).make_image(matrix)

	# rotate pixel space back to world space for saving
	image = image.transpose(Image.FLIP_TOP_BOTTOM)
	image = image.rotate(90)
	
	size = 4096, 4096
	image = image.resize(size, Image.ANTIALIAS)
	imagefile = imagename + '.png'
	image.save(imagefile)

def main():
	# move up out of tests
	os.chdir('..')
	query_list = {}
	query_list['lights'] = 'SELECT pos FROM Entity WHERE EntityClass = "Light" AND parent IS NULL;'
	query_list['player_spawns'] = 'SELECT pos FROM Entity WHERE EntityClass = "SpawnPoint" AND layer NOT LIKE "hendrik_waterworld";'
	query_list['spikers'] = 'SELECT pos FROM Brush WHERE Prefab Like "Objects/spawners/spiker.cgf";'
	query_list['deers'] = 'SELECT pos FROM Brush WHERE Prefab Like "Objects/spawners/randomdeer.cgf"'
	query_list['trucks'] = 'SELECT pos FROM Brush WHERE Prefab Like "Objects/spawners/f100truck.cgf"'
	query_list['towcar'] = 'SELECT pos FROM Brush where prefab LIKE "Objects/spawners/towcar.cgf";'
	query_list['quadbike'] = 'SELECT pos FROM Brush where prefab LIKE "Objects/spawners/quadbike.cgf";'
	query_list['boids'] ='SELECT pos FROM EntityArchetype where `group` IS NULL'
	
	for imagename, query in query_list.items():
		try:
			print "===================== Creating %s.png =======================" % imagename
			print "======================Query: %s " % query
			create_heatmap(imagename, query)
		except Exception, e:
			print e





if __name__ == '__main__':
	main()





