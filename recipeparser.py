#!/usr/bin/env python
# title           :recipeparser.py
# description     :This script takes a dictionary of recipe values {'ColumnName':['value', 'datatype']}
#  								 and will add every value and colum needed to the db. 
# author          :Chris Sprance / Entrada Interactive
# date            :06/02/15
# usage           : Formats all the recipe items
# notes           :
# python_version  :2.7.5
# ==============================================================================

from atlas_db_class import AtlasDbClass
import xml.etree.ElementTree as ET
import logging
import os




class RecipeParser(object):
    """This class contains all the methods to parse"""
    def __init__(self):
        super(RecipeParser, self).__init__()
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(filename='logs/recipeparser.log',level=0, format='')
        self.db = AtlasDbClass(database_name='testing.db')
        self.file = str()
        self.filetype = "xml"
        self.folder = str()
        try:
            self.p4 = self.db.p4
        except:
            pass
        self.db.commit_changes = True


    def start_parsing(self):
        '''Start Parsing The layer or layers'''
        if len(self.folder) > 1:
            self.open_from_folder()
        else:
            self.filter()
        # after everything has been parsed and inserted commit all the changes to the database
        if self.db.commit_changes is True:
            self.db.conn.commit()


    def filter(self):
        """
        Here we filter the layer send the layer info to be stored
        Then we filter the contents
        if it's a group we need to find all the Object tags inside of the Objects tag
        otherwise just send each object off to be fully parsed

        """
        etree = self.parseXML()
        
        print etree.findall('guide')
    

    # recurse children function
    def recurse_children(self, obj):
            #process the obj.attrib and add them to a dict
            for attrib in obj.attrib:
                # if the key exists append something unique to it
                if self.db.query_dict.has_key(attrib):
                    self.db.query_dict[attrib + '_' + obj.tag] = obj.attrib[attrib]
                else:
                    self.db.query_dict[attrib] = obj.attrib[attrib]
            # if the obj has any children send each child through
            if len(obj.getchildren()) > 0:
                # loop through all the children and add there attributes in to query dict
                for child in obj.getchildren():
                    self.recurse_children(child)


    def parseXML(self):
        """Parse the xml and return the xml tree back to us"""
        # lg.info( self.material_file)
        xml_file = open(self.file)
        tree = ET.parse(xml_file).getroot()
        return tree
        

    def open_from_folder(self):
        '''This method contains the code necesarry to open all .lyr if recursive arg is set to true it will recurse through all subdirectories'''
        for root, dirs, files in os.walk(self.folder):
            for file in files:
                if file.endswith(self.filetype):
                    self.file = os.path.join(root, file)
                    try:
                        self.filter()
                    except Exception, e:
                        self.logger.error( 'Cannot open %s Filetype: %s Error: %s' % (self.filetype, self.file, e))
                        pass

                        
def main():
    rp = RecipeParser()
    
    # rp.file = r'D:\perforce\Atlas\tests\atlas_test_layer.lyr'

    rp.file = 'D:\perforce\GameSDK\Scripts\Recipes\Recipes.xml'
    
    # kick off everything
    rp.start_parsing()


if __name__ == '__main__':
    main()
