#!/usr/bin/env python
# title           :cgfparser.py
# description     :This script takes a dictionary of recipe values {'ColumnName':['value', 'datatype']}
#  								 and will add every value and colum needed to the db. 
# author          :Chris Sprance / Entrada Interactive
# date            :06/02/15
# usage           : Grabs specific data points from a geometry file and insert it into the database
# notes           : 
# python_version  :2.7.5
# ==============================================================================

import logging
import os
import chunkfile
# from P4 import P4
from atlas_db_class import AtlasDbClass



class CgfParser(object):
    """This class contains all the methods to parse a Crytek geometry filetypeand give us relevant datapoints"""
    def __init__(self, db):
        super(CgfParser, self).__init__()
        self.db = db
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(filename='logs/cgfparser.log',level=logging.ERROR, format='')
        self.file = str()
        self.filetype = ('cgf','cga', 'chr', 'skin')
        self.folder = str()
        self.db.commit_changes = True
        self.p4 = self.db.p4 if self.db.use_p4 else None


    def start_parsing(self):
        '''Start Parsing The geometry file or files'''
        if len(self.folder) > 1:
            self.open_from_folder()
        else:
            self.filter()
        # after everything has been parsed and inserted commit all the changes to the database
        if self.db.commit_changes is True:
            self.db.conn.commit()


    def filter(self):
        """
        Here we filter the geometry file using chunkfile send the chunks off to be parsed
        """
        qdict = dict()
        cgf = [chunk for chunk in chunkfile.load_chunk_file(self.file).iter_chunks()]

        try:
            if self.db.use_p4:
                qdict['path'] = self.db.p4.run('fstat',self.file)[0]['depotFile']
            else:
                qdict['path'] = self.file


            # qdict['export_flags'] = self.file
            qdict['node_names'] = str()


            phys_count = list()
            for idx, chunks in enumerate(cgf):
                # iter_chunks() returns a tuple of the data and the position of struct
                chunk = chunks[0]

                # get the previous chunk this is because sometimes 
                # we only know what the mesh is from it's next chunk.
                prev_chunk = cgf[idx-1][0]

                # read the data about lods and the rendermesh
                if type(chunk) is chunkfile.node_chunk.NodeChunk and type(prev_chunk) is chunkfile.mesh_chunk.MeshChunk:
                    
                    if chunk.get_node_name().split("\0")[0][:5] == '$lod1':
                        qdict['lod1'] = prev_chunk.get_vertex_count()
                    
                    elif chunk.get_node_name().split("\0")[0][:5] == '$lod2':
                        qdict['lod2'] = prev_chunk.get_vertex_count()
                    
                    elif chunk.get_node_name().split("\0")[0][:5] == '$lod3':
                        qdict['lod3'] = prev_chunk.get_vertex_count()
                    
                    elif chunk.get_node_name().split("\0")[0][:5] == '$lod4':
                        qdict['lod4'] = prev_chunk.get_vertex_count()
                    
                    elif chunk.get_node_name().split("\0")[0][:5] == '$lod5':
                        qdict['lod5'] = prev_chunk.get_vertex_count()

                    elif chunk.get_node_name().split("\0")[0][:5] == '$lod6':
                        qdict['lod6'] = prev_chunk.get_vertex_count()
                        
                    elif chunkfile.chunk_utils.null_termin(chunk.get_node_name())[:5] != "'$phy":
                        qdict['rendermesh'] = prev_chunk.get_vertex_count()

                # read the data from the NodeChunk.get_node_name()
                if type(chunk) is chunkfile.node_chunk.NodeChunk:
                    qdict['node_names'] += ' | %s | ' % str(chunkfile.chunk_utils.null_termin(chunk.get_node_name())).replace("'", '')

                # read the data about the Physics proxy
                if type(chunk) is chunkfile.mesh_physics_data_chunk.MeshPhysicsDataChunk:
                    reader = chunkfile.chunk.DataReader(chunk.data)
                    header = chunk.read_header(reader)
                    data_length = header.find_field('nDataSize').value
                    physics_data_header = chunk.read_physics_data(reader, data_length)
                    geom_type = physics_data_header.find_field('GeomType').get_enum_string()
                    
                    # set the physics proxy geometry type (Box, cylinder, sphere etc)
                    qdict['proxy_type'] = geom_type
                    
                    if geom_type == "GEOM_TRIMESH":
                        num_trimesh_verts = chunk.read_trimesh_primitive_header(reader).find_field('m_nVertices').value
                        phys_count.append(num_trimesh_verts)
                    elif geom_type == "GEOM_CYLINDER":
                        phys_count.append(1)
                    else:
                        # print chunk.read_prims_count(reader).value
                        x =  (chunk.read_prims_count(reader).fields[0].value)
                        if type(chunkfile.chunk_utils.halffloat_to_intfloat(x)) is long:
                            phys_count.append(1)
                        else:
                            phys_count.append(int(chunkfile.chunk_utils.halffloat_to_intfloat(x)))

                
                    # qdict['phys'] = prev_chunk.get_vertex_count()
                
                # read the data about the MTL associated with the mesh
                if type(chunk) is chunkfile.mtl_name_chunk.MtlNameChunk:
                    reader = chunkfile.chunk.DataReader(chunk.data)
                    # print chunk.read_header_802(reader).fields[2].value.split("\0")[0]
                    header =  chunk.read_header_802(reader)
                    qdict['mtl_name'] = header.find_field('name').value.split("\0")[0]

            if len(phys_count) > 1 or 0 in phys_count:
                qdict['proxy_count'] = phys_count.__len__()
            else:
                try:
                    qdict['proxy_count'] = phys_count[0]
                except:
                    pass

            self.db.query_dict = self.db.start_insert(qdict)
            # star the query building and inserting off
            self.db.build_query('CGF')
            # lg.info(qdict)
        except Exception, e:
            self.logger.error(e)
            raise e
        

    def open_from_folder(self):
        '''This method contains the code necesarry to open all geometry files individualy or from a folder'''
        for root, dirs, files in os.walk(self.folder):
            for file in files:
                if file.endswith(self.filetype):
                    self.file = os.path.join(root, file)
                    try:
                        self.filter()
                    except Exception, e:
                        self.logger.error( 'Cannot open %s Filetype: %s Error: %s' % (self.filetype, self.file, e))

                        
def main():
    db = AtlasDbClass(database_name='atlas.db', use_p4=True)
    cp = CgfParser(db)
    
    # rp.folder = r'D:\perforce\GameSDK\Objects'

    # cp.file = r"D:\perforce\dev\GameSDK\Objects\natural\grass\field_grass\field_grass_tb_1m.cgf"
    cp.folder = r'D:\perforce\dev\GameSDK\Objects'
    
    # kick off everything
    cp.start_parsing()


if __name__ == '__main__':
    main()
