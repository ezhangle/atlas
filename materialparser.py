#!/usr/bin/env python
# title           :matparser.py
# description     :
# author          :Chris Sprance / Entrada Interactive
# date            :06/02/15
# usage           : This Script will be called from within the Atlas UI
# This Script parses a Crytek .mtl file and inserts it into a sqlite3 db
# using the AtlasDbClass from atlas_db_class
# notes           : 
# python_version  :2.7.5
# ==============================================================================

import xml.etree.ElementTree as ET
import logging
import os
from atlas_db_class import AtlasDbClass


class MaterialParser(object):
	"""This class contains all the methods to parse Crytek .mtl files into a dictionary to insert"""
	def __init__(self, db):
		super(MaterialParser, self).__init__()
		self.logger = logging.getLogger(__name__)
		logging.basicConfig(filename='logs/materialparser.log',level=logging.ERROR, format='')
		self.db = db
		self.material_file = str()
		self.material_folder = str()
		self.db.commit_changes = True
		self.p4 = self.db.p4 if self.db.use_p4 else None


	def start_parsing(self):
		'''Start Parsing The layer or layers'''
		if len(self.material_folder) > 1:
		    self.open_materials_from_folder()
		else:
		    self.filter_material()
		# after everything has been parsed and inserted commit all the changes to the database
		if self.db.commit_changes is True:
		    self.db.conn.commit()


	def filter_material(self):
		"""
		If it's a submaterial we need to find all the material tags inside of the submaterial tag and send those to be added

		"""
		material_etree = self.parseXML()
		try:
			# If it's a submaterial we need to find all the material tags inside of the submaterial tag and send those to be added
			if material_etree.find('SubMaterials') != None:
				# materials is a list containing etree objects
				materials = material_etree.find('SubMaterials').findall('Material')

				# loop through the materials adding the submat info to the dict
				for idx, material in enumerate(materials):
					material.attrib['issubmat'] = 'True'
					material.attrib['submatid'] = str(idx)

				# loop through the materials list and send each material to create_query_dict_from_material()
				for material in materials:
					self.create_query_dict_from_material(material)
			else:
				# if it has no submaterials send just the material etree off to create the query dict
				self.create_query_dict_from_material(material_etree)
		except Exception, e:
			self.logger.error(e)
			raise e




	def create_query_dict_from_material(self, material):
		
		# get the textures from the material etree
		textures = material.find('Textures').findall('Texture')

		# loop through the textures adding there attrib['Map'].replace( ' ', '') as the column name
		# and the attrib['File'] as the value to the material.attrib dict
		for texture in textures:
			# column name and value for texture
			material.attrib['texture_' + texture.attrib['Map'].replace(' ', '_')] = texture.attrib['File']

		# we need to set some values here
		if self.db.use_p4:
		 	material.attrib['filepath'] = self.p4.run('fstat',self.material_file)[0]['depotFile']
		else:
		 	material.attrib['filepath'] = self.material_file
		
		# create a unique id for everything by using it's filepath and submatid or just the file path if it's a single mat
		material.attrib['id'] = (self.material_file + '#' + str(material.attrib['submatid']) if material.attrib.has_key('issubmat') == True else self.material_file)
		
		# build our query dict
		self.db.query_dict = self.db.start_insert(material.attrib)
		# set our primary key
		self.db.primary_key = 'id'
		# star the query building and inserting off
		self.db.build_query('Materials')




	def parseXML(self):
		"""Parse the xml and return the xml tree back to us"""
		tree = ET.parse(self.material_file).getroot()
		return tree
		

	def open_materials_from_folder(self):
		'''This method contains the code necesarry to open all .lyr if recursive arg is set to true it will recurse through all subdirectories'''
		for root, dirs, files in os.walk(self.material_folder):
		    for file in files:
		        if file.endswith(".mtl"):
		            self.material_file = os.path.join(root, file)
		            try:
		            	self.filter_material()
		            except Exception, e:
		            	self.logger.error( 'Cannot open Material File: %s Error: %s' % (self.material_file, e))
		            	pass

		            	
def main():
	# db = AtlasDbClass(database_name='penis.db', use_p4=False)
	# mp = MaterialParser(db)
	# # mp.material_file = 'D:\perforce\GameSDK\Objects\\atlas\\atlas_test_single_mat.mtl'
	# mp.material_file = "D:\perforce\Atlas\tests\material_test_data\teste_mat.mtl"
	# # mp.material_folder = 'D:\perforce\GameSDK\Objects\\atlas'
	
	# # kick off everything
	# mp.start_parsing()
	pass


if __name__ == '__main__':
	main()
